public class CreatingThreadDemo {

    // Главный поток
    public static void main(String[] args) {

        System.out.println("Починається виконання програми");

        // Створення обєкту класу Thread  для запуску
        // дочірнього потоку на виконнання
        Thread t = new Thread(new MyThread());
        System.out.println("Запускається дочірній потік");

        // Запуск дочірноьго потоку
        t.start();

        for (int k = 0; k <= 5; k++) {
            System.out.println("Головний потік: \t" + (char)('A' + k));
            try {
                // Затримка в виконанні
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
                System.out.println("Переривання основного потоку");
            }
        }

        System.out.println("Вионання програми завершено");
    }
}
