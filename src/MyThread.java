public class MyThread implements Runnable {

    // Описание метода Run() - программный код потока
    @Override
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println("Дочерный поток: \t" + i);
            try {
                // Задержка при выполнении потока
                Thread.sleep(1200);
            }
            // обработка ошибки
            catch (InterruptedException e) {
                System.out.println("Прерывание дочернего потока");
            }
        }
    }
}
